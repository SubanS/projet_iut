<?php

    //On demande l'insersion du ficher config de la bse de donnée.
    require_once('../config/config_bdd.php');

    //On initialise une fonction qui prends en paramètre la réussite, le mot de passe et le login de la tentative que nous utiliserons à chaque reussite ou echec, ainsi que les variables de connexion à la base de données.
    function activite($mdp, $log): void
    {
        //On convertie les caractères spéciaux en caratères spéciaux html.
        $adr_ip = $_SERVER['REMOTE_ADDR'];

        //Ecriture dans le fichier csv des logs
        $dt = time();
        $dt_format = date('d/m/Y H:i:s', $dt);
        $data = array($mdp, $log, $adr_ip, $dt_format);
        $file = "log.csv";
        $fp=fopen($file,"a+");
        fputcsv($fp, $data);
        fclose($fp);
    }

    //On vérifie si un formulaire à été envoyé.
    if(isset($_POST['Envoyer'])){
        //Si le champ "login" n'est pas vide, on continue.
        if(!empty($_POST['login'])){
            //Si le champ "mdp" n'est pas vide, on continue.
            if(!empty($_POST['mdp'])){
                //On transforme les caractère en caractères spéciaux.
                $login = htmlspecialchars($_POST['login']);
                $mdp = htmlspecialchars($_POST['mdp']);
                //On selectionnne le login et le type d'utilisateur si une ligne correspond au "login" entrée et si le mot de passe entrée puis crypté corespond à celui existant pour ce "login".
                //On prépare la requête avec des valeurs indéfinies.
                $verif = "SELECT login FROM users WHERE login = ?";
                //On prépare la requete avec des valeurs indéfinie.
                $verifp = mysqli_prepare($connexion,$verif);
                //On définit le type de valeur à entrer et on execute la requete.
                mysqli_stmt_bind_param($verifp,'s', $login);
                //On l'execute.
                mysqli_stmt_execute($verifp);
                // On récupère l'objet renvoyé par la requête préparée
                $result = mysqli_stmt_get_result($verifp);

                //Si la requête n'est pas vide.
                if(mysqli_num_rows($result) != 0){
                    //On selectionnne le login et le type d'utilisateur si une ligne correspond au "login" entrée et si le mot de passe entrée puis crypté corespond à celui existant pour ce "login".
                    //On prépare la requête avec des valeurs indéfinies.
                    $verif2 = "SELECT login FROM users WHERE login = ? AND mdp = ?";
                    //On prépare la requete avec des valeurs indéfinie.
                    $verifp2 = mysqli_prepare($connexion,$verif2);
                    //On crypte le mot de passe.
                    $mdpfin = md5($mdp);
                    //On définit le type de valeur à entrer et on execute la requete.
                    mysqli_stmt_bind_param($verifp2,'ss', $login, $mdpfin);
                    //On l'execute.
                    mysqli_stmt_execute($verifp2);
                    // On récupère l'objet renvoyé par la requête préparée
                    $result2 = mysqli_stmt_get_result($verifp2);

                    if(mysqli_num_rows($result2) != 0){
                        //On crée une session qui contien son "login" et son type d'utilisateur.
                        session_start();

                        //On récupere les valeurs de la requete.
                        $data2 = $result2->fetch_assoc();

                        $_SESSION["user"] = ["login"=>$login,"type_user"=>$data2['type_user']];
                        //Si l'utilisateur n'est pas un simple utilisateur.
                        if ($data2['type_user'] != 'user'){  
                            //On le redirige vers le panel "admin" et on ferme la page.
                            header('Location: ../page_admin.php');
                            die();
                        //Si l'utilisateur est un utilisateur simple
                        } else {  
                            //On le redirige vers la page d'acceuil
                            header('Location: ../index.php');
                            die();
                        }
                    }else{
                        //On insert les logs avec la fonction "activite".
                        activite($mdp, $login);
                        //Si le champ "mdp" ne correspond pas au login de la base de donnée, on le redirige vers la page de connection avec une erreur et on ferme la page.
                        header('Location: ../connexion.php?err=u_ou_mdp_faux');
                        die();
                    }
                }else{
                    //On insert les logs avec la fonction "activite".
                    activite($mdp, $login);
                    //Si le champ "login" est introuvable dans la base de donnée, on le redirige vers la page de connection avec une erreur et on ferme la page.
                    header('Location: ../connexion.php?err=u_ou_mdp_faux');
                    die();
            }
            }else{
                //Si le champ "mdp" est vide, on le redirige vers la page de connection avec une erreur et on ferme la page.
                header('Location: ../connexion.php?err=mdp_vide');
                die();
            }
        }else{
            //Si le champ "mdp" est vide, on le redirige vers la page de connection avec une erreur et on ferme la page.
            header('Location: ../connexion.php?err=login_vide');
            die();
        }
    }

?>