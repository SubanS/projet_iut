module com.example.apli {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires junit;
    requires org.testng;

    opens com.example.apli to javafx.fxml;
    exports com.example.apli;

    exports modele;
    exports vue;
    exports controleur;
}