package modele;
import vue.HBoxDistances;
import vue.HBoxMembre;
import vue.HBoxScenario;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class Graphe {
    /*Un objet de type graphe est caractérisé par:
     * une liste de sommet
     * un dictionnaire de la forme: clé = sommet et valeur = degré entrant
     * une matrice de voisin
     * une liste de sources*/

    ArrayList<String> listeSommet;
    TreeMap<String,Integer> degreEntrant;
    ArrayList<ArrayList<String>> voisin;
    ArrayList<String> sources;
    TreeMap<String,ArrayList> map;
    ArrayList<ArrayList> finale;

    public Graphe() { // on crée un graphe sur un scenario et des membres choisis
        listeSommet = new ArrayList<>();
        degreEntrant = new TreeMap<>();
        voisin = new ArrayList<>();
        sources = new ArrayList<>();
        finale = new ArrayList<>();
    }

    public Graphe création (HBoxScenario scenario, HBoxMembre membre){
        /*
        A partir d'un scénario et de la liste de membre, la fonction rempli les champs du graphe
        * Paramètre: un scénario, un tableau de membre
        * Retour: un objet de type graphe
        */

        Graphe graphe = new Graphe();
        graphe.adDegré(scenario,membre);        //remplis le dictionnaire de degrés entrants
        graphe.adSommet(scenario,membre);    //remplis la liste des sommets
        graphe.adVoisin(scenario,membre);   //remplis la liste des voisins
        graphe.adSources();                //remplis la liste des sources
        return graphe;
    }

    public void adDegré(HBoxScenario scenario,HBoxMembre membre){
        /* fonction pour créer le dictionnaire de degré entrant des sommets*/
        Iterator<String> iterator = scenario.getMembres().iterator();
        while (iterator.hasNext()){
            String clé = iterator.next();
            degreEntrant.put(membre.getVilleMembre(clé),0);     //on crée les clées de map
        }

        iterator = scenario.getMembres().iterator();
        while (iterator.hasNext()){
            String nom1 = membre.getVilleMembre(iterator.next());  // on parcours la liste des membres du scénario

            Iterator<String> iterator2 =scenario.getAcheteurs().iterator();
            while (iterator2.hasNext()){
                String nom2 = membre.getVilleMembre(iterator2.next());     // on parcours la liste des acheteurs du scénario

                if (nom1.equals(nom2)){                                 // a chaque fois que le nom du membre est present en tant qu'acheteur,
                    int deg = degreEntrant.get(nom1);                  // cela traduit une nouvelle transaction,
                    degreEntrant.replace(nom1,deg+1);                 // il a un degré entrant supplementaire.
                }
            }
        }
    }

    public Integer fact(Integer var){
        /* fonciton pour calculer la factorielle d'un nombre donnée*/
        int fact=1;
        for (int i=1; i<=var; i++){
            fact = fact*i;
        }
        return fact;
    }

    public void adSommet(HBoxScenario scenario,HBoxMembre membre){
        /*La fonction créer la liste des sommets du graphe*/
        Iterator<String> itarator = degreEntrant.keySet().iterator();
        while (itarator.hasNext()) {
            listeSommet.add(itarator.next());
        }
    }

    public void adVoisin (HBoxScenario scenario, HBoxMembre membre){
        /* fonction pour créer la liste d'adjacence des sommets*/
        int i = 0;
        while (i <listeSommet.size()){
            voisin.add(new ArrayList<>());  //on crée un sous tableaux pour chaque ville
            i+=1;
        }
        Iterator<String> vendeur = scenario.getVendeurs().iterator();    //on itère sur les listes
        Iterator<String> acheteur = scenario.getAcheteurs().iterator(); //de vendeurs et achteurs
        while(vendeur.hasNext() && acheteur.hasNext()){
            String villeAcheteur = membre.getVilleMembre(acheteur.next());
            String villeVendeur = membre.getVilleMembre(vendeur.next());
            voisin.get(listeSommet.indexOf(villeVendeur)).add(villeAcheteur);    //on remplit la matrice
        }
    }

    public void adSources(){
        /*La fonction calcul la liste des sources du graphe*/

        Iterator<String> clé = degreEntrant.keySet().iterator();

        while (clé.hasNext()) {
            String ville = clé.next();
            if (this.degreEntrant.get(ville) == 0) {  //Si le degré entrant du sommet est null,
                this.sources.add(ville);              // alors il entre dans la liste des sources
            }
        }
    }

    public ArrayList<ArrayList<String>> generatePerm(ArrayList<String> original) {
        /* la fonction genere toute les permutations possible d'une liste donnée*/

        if (original.size() == 0) {                                   //si la taille de la liste atteint 0,
            ArrayList<ArrayList<String>> result = new ArrayList<>(); // alors on a toute les possibilité,
            result.add(new ArrayList<String>());                    // on returne le résultat
            return result;
        }
        String firstElement = original.remove(0);
        ArrayList<ArrayList<String>> returnValue = new ArrayList<>();
        ArrayList<ArrayList<String>> permutations = generatePerm(original);
        for (ArrayList <String> smallerPermutated : permutations) {
            for (int index=0; index <= smallerPermutated.size(); index++) {
                ArrayList <String> temp = new ArrayList<String>(smallerPermutated); //on ajoute une portion de la liste original
                temp.add(index, firstElement);                                     //à une nouvelle sous liste,
                returnValue.add(temp);                                            //elle meme ajouté à une liste finale
            }
        }
        return returnValue;
    }

    public int getLongItineraire(ArrayList itineraire) throws IOException {
        /*
        La fonction calcule la longueur d'un itinéraire
         */

        int longueur = 0;

        HBoxDistances distance = new HBoxDistances();
        File distances = new File ("src/main/resources/distances.txt");
        distance = distance.lectureDistance(distances);

        int ville1 = 0;
        int ville2 = 1;

        while (ville2<itineraire.size()) {
            // En parcourant l'itineraire, on ajoute a la distance finale
            // la longueur qui separe une ville et la suivante
            longueur += Integer.parseInt((String)(distance.getDistance((String) itineraire.get(ville1), (String) itineraire.get(ville2))));
            ville1+=1;
            ville2+=1;
        }
        return longueur;
    }

    public ArrayList<String> plusCourt (ArrayList<ArrayList> toutChemin) throws IOException {
        /*La fonction renvoie l'itineraire qui a la distance la plus courte*/

        int mini= getLongItineraire(toutChemin.get(0));  // on initialise avec
        ArrayList plusCourt = toutChemin.get(0);        // les valeurs du premier chemin trouvé

        for (ArrayList<String> chemin : toutChemin){

            int distance = getLongItineraire(chemin);    // on parcours les chemins
            if (distance<mini){                         // en calculant la longueur de chacun
                mini=distance;                          // on redéfinie le chemin le plus court
                plusCourt=chemin;
            }
        }
        return plusCourt;
    }

    public ArrayList<ArrayList> toutChemin(){
        /*La fonciton renvoit tout les chemins possible d'un graphe*/

        ArrayList<String> chemin = new ArrayList();                                      // chemin crée
        TreeMap<String,ArrayList> predecesseurMap = (TreeMap)predecesseur().clone();    //dictionnaire de prédesseur
        ArrayList<String> source = new ArrayList();                                    //sources étuidiées
        ArrayList<ArrayList> niveau = new ArrayList();                                //chemin sectioné en niveau
        ArrayList<ArrayList> listeTemp = new ArrayList();                            // premiere liste de chemin
        ArrayList<ArrayList> finale = new ArrayList();                              //liste regroupant tout les chemins

        while (chemin.size()!=predecesseurMap.size()) {
            //Tant que le chemin n'a pas tout ses sommets

            for (String key1 : predecesseurMap.keySet()) {
                if (predecesseurMap.get(key1).size() == 0 && !chemin.contains(key1)) { // on cherche les sources
                    source.add(key1);                                        // si le sommet est de degré entrant null
                }
            }

            for (String s : source){         //si la source n'est pas dans le chemin,
                if (!chemin.contains(s)) {  // alors on l'y ajoute
                    chemin.add(s);
                }
            }

            ArrayList<String> sourceBis = (ArrayList) source.clone();
            for (ArrayList<String> perm : generatePerm(sourceBis)){
                ArrayList tempo = new ArrayList();
                for (String src : perm){                              // on ajoute dans une liste temporaire,
                    if (!tempo.contains(src)) {                   // mais dans un ordre differents
                        tempo.add(src);
                    }
                }
                niveau.add(tempo);
            }

            for (String src : source){
                for (String key2 : predecesseurMap.keySet()) {
                    if (predecesseurMap.get(key2).contains(src)) { //On retire du dictionnaire
                        predecesseurMap.get(key2).remove(src);     //les sommets visités
                    }
                }
            }
            source.clear();        // on vide la liste pour y accueilir de nouvelles sources
        }


        for (ArrayList nv :  niveau){
            for (int i = 0; i<fact(nv.size()); i++){
                ArrayList lst = new ArrayList();     // on créer le nombre de listes nécessaire
                lst.add(nv);                        //avec les valeurs des listes temporaire precedentes
                listeTemp.addAll((ArrayList) lst.clone());
            }
        }

        for (ArrayList<String> ville1 : listeTemp){

            ArrayList<String> missing = new ArrayList();  //on complete chaque liste avec les valeurs manquantes

            for (ArrayList ville2 : niveau){

                if (!ville1.contains(ville2.get(0))){
                    ville1.addAll(ville2);
                }
            }

            for (String src : ville1){
                if (!getSources().contains(src)) {  //on repère les sommets à permuter
                    missing.add(src);
                }
                else{break;}
            }
            int index= missing.size()-1;
            for(String mis : missing ){

                ville1.remove(mis);
                ville1.set(ville1.indexOf(mis),missing.get(index));  //On effectue des permutations
                index-=1;                                           // afin d'obtenir de nouveaux chemins
            }
            if (!finale.contains(ville1)){           //on ajoute à la liste finale de chemins
                finale.add(ville1);
            }
        }
        for(ArrayList ville : finale){
            ArrayList velizy = new ArrayList<>();
            velizy.add("Velizy");                     //On ajoute la ville de Vélizy
            velizy.addAll((ArrayList)ville.clone());    //au début et la fin
            ville.clear();                            //de chaque itinéraire
            ville.addAll(velizy);
            ville.add("Velizy");
        }
        return finale;
    }

    public TreeMap predecesseur (){
        /*La fonction renvoie un dictionnaire avec en clé, un sommet et en valeur une liste de prédécesseurs*/

        TreeMap<String,ArrayList> mapPredecesseur = new TreeMap();

        for (String ville1 : listeSommet) {                                                //pour chaque sommet
            ArrayList<String> ListePredecesseur = new ArrayList();                       // s'il est present dans une liste de voisinage
            for (String ville2 : listeSommet) {                                        // mais que ce n'est pas réciproque
                if (voisin.get(listeSommet.indexOf(ville2)).contains(ville1)) {      // alors c'est un prédécesseur
                    ListePredecesseur.add(listeSommet.get(listeSommet.indexOf(ville2)));
                }
                mapPredecesseur.put(ville1,ListePredecesseur);
            }
        }
        return mapPredecesseur;
    }



    /*ACCESSEURS*/

    public ArrayList getSommet () {
        /*sommets du graphe*/
        return listeSommet;
    }

    public TreeMap<String, Integer> getdegré () {
        /* dicctionaire de degré entrant*/
        return degreEntrant;
    }

    public List getVoisins () {
        /*liste d'adjacence*/
        return voisin;
    }

    public ArrayList<String> getSources () {
        /*sources initiales du graphe*/
        return sources;
    }

    /*AFFICHAGE*/

    public String toString () {
        return "sommets" + listeSommet.toString() + "\n" + "entrants" + degreEntrant.toString() + "\n" + "voisins" + voisin + "\n" + "sources" + sources;
    }
}