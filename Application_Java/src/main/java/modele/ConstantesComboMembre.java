package modele;

public interface ConstantesComboMembre {

    /* Constantes permettant de peupler la comboBox de la vue Membre */

    public final String INTITULE_VILLE_1 = "Amiens";
    public final String INTITULE_VILLE_2 = "Angers";
    public final String INTITULE_VILLE_3 = "Biarritz";
    public final String INTITULE_VILLE_4 = "Bordeaux";
    public final String INTITULE_VILLE_5 = "Brest";
    public final String INTITULE_VILLE_6 = "Calais";
    public final String INTITULE_VILLE_7 = "Cherbourg";
    public final String INTITULE_VILLE_8 = "Clermond_Fd";
    public final String INTITULE_VILLE_9 = "Dijon";
    public final String INTITULE_VILLE_10 = "Grenoble";
    public final String INTITULE_VILLE_11 = "Le_Havre";
    public final String INTITULE_VILLE_12 = "Lille";
    public final String INTITULE_VILLE_13 = "Lyon";
    public final String INTITULE_VILLE_14 = "Marseille";
    public final String INTITULE_VILLE_15 = "Montpellier";
    public final String INTITULE_VILLE_16 = "Nancy";
    public final String INTITULE_VILLE_17 = "Nantes";
    public final String INTITULE_VILLE_18 = "Nice";
    public final String INTITULE_VILLE_19 = "Paris";
    public final String INTITULE_VILLE_20 = "Perpignan";
    public final String INTITULE_VILLE_21 = "Reims";
    public final String INTITULE_VILLE_22 = "Rennes";
    public final String INTITULE_VILLE_23 = "Rouen";
    public final String INTITULE_VILLE_24 = "Saint-Etienne";
    public final String INTITULE_VILLE_25 = "Strasbourg";
    public final String INTITULE_VILLE_26 = "Toulouse";
    public final String INTITULE_VILLE_27 = "Tours";
    public final String INTITULE_VILLE_28 = "Vichy";
    public final String INTITULE_VILLE_29 = "Velizy";

    public final String [] ITEM_COMBOBOX_MEMBRE ={INTITULE_VILLE_1, INTITULE_VILLE_2, INTITULE_VILLE_3, INTITULE_VILLE_4, INTITULE_VILLE_5, INTITULE_VILLE_6, INTITULE_VILLE_7,
            INTITULE_VILLE_8, INTITULE_VILLE_9, INTITULE_VILLE_10, INTITULE_VILLE_11, INTITULE_VILLE_12, INTITULE_VILLE_13, INTITULE_VILLE_14, INTITULE_VILLE_15, INTITULE_VILLE_16,
            INTITULE_VILLE_17, INTITULE_VILLE_18, INTITULE_VILLE_19, INTITULE_VILLE_20, INTITULE_VILLE_21, INTITULE_VILLE_22, INTITULE_VILLE_23, INTITULE_VILLE_24, INTITULE_VILLE_25,
            INTITULE_VILLE_26, INTITULE_VILLE_27, INTITULE_VILLE_28, INTITULE_VILLE_29
    };




}
