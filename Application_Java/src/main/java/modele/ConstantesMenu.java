package modele;

public interface ConstantesMenu {

    /* Constantes permettant de peupler les éléments du menu de naviguation */

    public final String INTITULE_ACCUEIL = "_" + "Page D'Accueil";
    public final String INTITULE_SCENARIO = "_" + "Scénarios";
    public final String INTITULE_ITINERAIRE = "_"+ "Itinéraires";
    public final String INTITULE_MEMBRE = "_" + "Membres";
    public final String [] ITEM_MENU = {INTITULE_ACCUEIL,INTITULE_SCENARIO, INTITULE_ITINERAIRE, INTITULE_MEMBRE};

}
