package modele;

public interface ConstantesComboScenario {

    /* Constantes permettant de peupler la comboBox de la vue Scenario */

    public final String INTITULE_SCENARIO_0 = "scenario_0";
    public final String INTITULE_SCENARIO_1 = "scenario_1_1";
    public final String INTITULE_SCENARIO_2 = "scenario_1_2";
    public final String INTITULE_SCENARIO_3 = "scenario_2_1";
    public final String INTITULE_SCENARIO_4 = "scenario_2_2";

    public final String [] ITEM_COMBOBOX_SCENARIO = {INTITULE_SCENARIO_0,INTITULE_SCENARIO_1, INTITULE_SCENARIO_2, INTITULE_SCENARIO_3, INTITULE_SCENARIO_4};


}
