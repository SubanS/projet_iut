package controleur;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import modele.Graphe;
import vue.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Controleur implements EventHandler {

    private Graphe graphe;
    private String selectionCombo;
    private String selectionVilleDepart;
    private String selectionVilleArrive;
    private HBoxScenario hBoxScenarioChoisi;

    @Override
    public void handle(Event event) {

        // Creation d'objet de chaque vue
        HBoxScenario scenarioControleur = VBoxRoot.getScenario();
        HBoxDistances distanceControleur = VBoxRoot.getDistance();
        HBoxMembre membreControleur = VBoxRoot.getMembre();


        // la source de event est une ComboBox
        if (event.getSource() instanceof ComboBox<?>) {

            selectionCombo = (String) ((ComboBox<?>) event.getSource()).getSelectionModel().getSelectedItem(); // récupère la valeur de la ComboBox selectionné

           /* Controleur de la vue HBoxScenario */
           if ( selectionCombo.equals("scenario_0") || selectionCombo.equals("scenario_1_1") || selectionCombo.equals("scenario_1_2") || selectionCombo.equals("scenario_2_1") || selectionCombo.equals("scenario_2_2") ) {
               hBoxScenarioChoisi = new HBoxScenario();
               File scenarioChoisi = new File("src/main/resources/" + selectionCombo + ".txt"); // lecture du scenario choisi
               try {
                   hBoxScenarioChoisi = hBoxScenarioChoisi.lectureScenario(scenarioChoisi);
                   scenarioControleur.getAreaTextScenario().setText(hBoxScenarioChoisi.toString());
                   Graphe graphe1 = new Graphe();
                   graphe = graphe1.création(hBoxScenarioChoisi, membreControleur); // création du graphe avec le scenario choisi
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }


            /* Controleur de la vue HBoxDistance */
            // ComboBox Itinéraire
            if ( ( (ComboBox<?>) event.getSource() ).getUserData().equals("comboItineraire") ) {
                if ( ( (ComboBox<?>) event.getSource() ).getSelectionModel().getSelectedItem().equals("Premier") ) {
                    try {
                        distanceControleur.getAreaTextItineraire().setText( "Itinéraire du " + selectionCombo + " : "+ graphe.toutChemin().get(0)+ "\n" + "\n"
                        + "Distance total de ton itinéraire : " + this.graphe.getLongItineraire(graphe.toutChemin().get(0)) +   " km." + "\n" + "\n"
                        + "Tu peux retrouver la distance entre 2 villes de cet itinéraire dans la liste déroulante ci-dessous.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if ( ( (ComboBox<?>) event.getSource() ).getSelectionModel().getSelectedItem().equals("Aléatoire") ) {
                    Random random = new Random(); // élément permettant de récuperer une valeur aléatoire de la liste
                    ArrayList grapheAleatoire = graphe.toutChemin().get(random.nextInt(graphe.toutChemin().size())); // on génère l'itinéraire aléatoire
                    try {
                        distanceControleur.getAreaTextItineraire().setText("Itinéraire " + selectionCombo + " : " + grapheAleatoire +"\n" + "\n"
                        + "Distance total de ton itinéraire : " + graphe.getLongItineraire(grapheAleatoire) + " km." + "\n" + "\n"
                        + "Tu peux retrouver la distance entre 2 villes de cet itinéraire dans la liste déroulante ci-dessous." );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if ( ( (ComboBox<?>) event.getSource() ).getSelectionModel().getSelectedItem().equals("Plus Court") ) {
                    try {
                        distanceControleur.getAreaTextItineraire().setText( "Itinéraire le " + selectionCombo + " : " + graphe.plusCourt(graphe.toutChemin()) +"\n" + "\n"
                        + "Distance total de ton itinéraire : " + graphe.getLongItineraire( ( graphe.plusCourt(graphe.toutChemin() ) )) + " km." + "\n" + "\n"
                        + "Tu peux retrouver la distance entre 2 villes de cet itinéraire dans la liste déroulante ci-dessous." );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (((ComboBox<?>) event.getSource()).getSelectionModel().getSelectedItem().equals("Tout les itinéraires")) {
                    distanceControleur.getAreaTextItineraire().setText("Voici tous les itinéraires de ce scénario, " +
                    graphe.toutChemin().size() + " au total :" + "\n" );
                    for (ArrayList chemin: graphe.toutChemin()){
                        distanceControleur.getAreaTextItineraire().appendText(chemin.toString() + "\n");
                    }
                }
            }
            // ComboBox distVille
            if ( ( (ComboBox<?>) event.getSource()).getUserData().equals("comboVilleDepart") || ((ComboBox<?>) event.getSource()).getUserData().equals("comboVilleArrive") ){
                if ( ( (ComboBox<?>) event.getSource()).getUserData().equals("comboVilleDepart")){
                    selectionVilleDepart = (String) ((ComboBox<?>) event.getSource()).getSelectionModel().getSelectedItem(); // recuperation de l'element selectionné dans la premiere ComboBox
                }
                if ( ( (ComboBox<?>) event.getSource()).getUserData().equals("comboVilleArrive")){
                    selectionVilleArrive = (String) ((ComboBox<?>) event.getSource()).getSelectionModel().getSelectedItem(); // recuperation de l'element selectionné dans la seconde ComboBox
                }
                if (selectionVilleDepart != null && selectionVilleArrive != null){
                    distanceControleur.getAreaTextDistVille().setText("La distance entre " + selectionVilleDepart + " et " + selectionVilleArrive + " est de " +
                            distanceControleur.getDistance(selectionVilleDepart, selectionVilleArrive) + " km."
                    );
                }
            }

            /* Controleur de la vue HBoxMembre */
            if ( ((ComboBox<?>) event.getSource() ).getUserData().equals("comboVille") ){
                membreControleur.getAreaTextMembre().setText(membreControleur.getMembreVille(selectionCombo)); //affiche les membres d'une ville
            }
        }
    }
}
