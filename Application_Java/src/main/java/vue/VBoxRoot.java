package vue;

import controleur.Controleur;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import modele.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class VBoxRoot extends VBox implements ConstantesMenu {

    private static HBoxScenario scenario;
    private static HBoxDistances distance;
    private static HBoxMembre membre;
    private MenuBar menuBarApli;
    private static HBoxPageAccueil pageAccueil;
    private static Controleur controleur;

    public VBoxRoot() throws IOException {
        super(50);

        // Creation Controleur
        controleur = new Controleur();

        // Lecture fichier Scenario
        scenario = new HBoxScenario();
        File scenarios = new File("src/main/resources/scenario_1_2.txt");
        scenario = scenario.lectureScenario(scenarios);


        // Lecture fichier Distance
        distance = new HBoxDistances();
        File distances = new File ("src/main/resources/distances.txt");
        distance = distance.lectureDistance(distances);


        // Lecture fichier Membre
        membre = new HBoxMembre();
        File membreFile = new File("src/main/resources/membres_APLI.txt");
        membre = membre.lectureMembre(membreFile);


        // Creation StackPane qui empile les vues
        StackPane stackPane = new StackPane();
        pageAccueil = new HBoxPageAccueil();
        stackPane.getChildren().addAll(pageAccueil,scenario, distance, membre);


        // Ajout Vue dans un Tableau
        Node [] components = new Node[4];
        components [0] = pageAccueil;
        components [1] = scenario;
        components [2] = distance;
        components [3] = membre;


        // Creation Menu
        menuBarApli = new MenuBar();
        Menu menu = new Menu(INTITULE_ACCUEIL);
        menu.setMnemonicParsing(true);
        menuBarApli.getMenus().add(menu);


        // Ajout item des vues dans le menu
        for (int i=0; i < ITEM_MENU.length; i++){
            MenuItem menuItem = new MenuItem(ITEM_MENU[i]);
            menuItem.setUserData(i);
            // Mettre en relation bouton et vue
            menuItem.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    int data =(int)((MenuItem) actionEvent.getSource()).getUserData();
                    int last = stackPane.getChildren().size()-1;
                    while (stackPane.getChildren().get(last)!=components[data]){
                        stackPane.getChildren().get(0).toFront();
                    }
                    menu.setText(ITEM_MENU[data]);
                }
            });
            menu.getItems().add(menuItem);
        }

        // Avoir la page d'accueil en premier lors du lancement de l'application
        List<Node> listScrollPane = stackPane.getChildren();
        while(!listScrollPane.get(0).equals(components[0])){
            listScrollPane.get(0).toFront();
        }
        listScrollPane.get(0).toFront();


        // Pour le CSS, afin d'eviter les problèmes de superposition
        distance.setId("opaque");
        scenario.setId("opaque");
        membre.setId("opaque");
        pageAccueil.setId("opaque");


        // Ajout à la fenetre graphique
        this.getChildren().addAll(menuBarApli, stackPane);

    }

    /* Acceseur */
    public static HBoxScenario getScenario(){
        return scenario;
    }
    public static HBoxDistances getDistance(){return distance;}
    public static HBoxMembre getMembre(){return membre;}
    public static HBoxPageAccueil getPageAccueil(){return pageAccueil;}
    public static Controleur getControleur() {
        return controleur;
    }
}
