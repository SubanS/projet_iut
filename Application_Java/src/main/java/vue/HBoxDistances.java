package vue;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import modele.ConstantesComboDistances;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class HBoxDistances extends HBox implements ConstantesComboDistances {

    ArrayList<ArrayList<Integer>> tabDistances;
    List nomVille;
    TextArea areaTextItineraire;
    TextArea areaTextDistVille;

    public HBoxDistances(){
        /* L'objet Distance est défini par la liste des villes
                      et la matrice de leur distance */
        tabDistances = new ArrayList (); //matrice des distances entre les villes
        nomVille = new ArrayList<>(); //tableau des noms de villes étudiées


        /* Label Explication page Iitnéraire */
        Label labelExplicationIti = new Label("Bienvenue dans cette nouvelle étape de ton voyage. Ici, tu as la possibilité de choisir l'itinéraire que tu souhaites." +"\n" +
                "Pour cela, tu as le choix entre 4 possibilités dépendant du scénario choisi :" + "\n" +
                "- Le Premier choix, est le premier itinéraire que notre équipe de chercheur a découvert." +"\n" +
                "- Le Deuxième choix, est un itinéraire choisi aléatoirement parmi notre base de données." + "\n" +
                "- Le Troisième est l'itinéraire le plus rapide, parmi toute les solutions trouvées" + "\n" +
                "- Le Quatrième et dernier choix, est l'ensemble des itinéraires que notre équipe de chercheur à trouvés "+ "\n" +
                "Tu as donc ton itinéraire pour ton aventure, à toi de jouer.")
        ;


        /* ComboBox permettant de choisir l'itinéraire voulu */
        ComboBox comboItineraire = peupleComboBox(ITEM_COMBOBOX_ITINERAIRE);
        comboItineraire.setUserData("comboItineraire");
        comboItineraire.setValue("Choisis un itinéraire.");
        comboItineraire.setOnAction(VBoxRoot.getControleur());


        /* Le text area permettant d'afficher l'itinéraire voulu */
        areaTextItineraire = new TextArea();
        areaTextItineraire.setWrapText(true);
        areaTextItineraire.setPrefRowCount(7);
        areaTextItineraire.setPrefColumnCount(20);


        /* 2 ComboBox contenant chacune 1 ville pour avoir la distance entre elle */
        // 1ere comboBox pour la ville de départ
        ComboBox comboVilleDepart = peupleComboBox(ITEM_COMBOBOX_DIST_VILLE);
        comboVilleDepart.setUserData("comboVilleDepart");
        comboVilleDepart.setValue("Choisi la première ville.");
        comboVilleDepart.setOnAction(VBoxRoot.getControleur());
        // 2eme ComboBox pour la ville d'arrivée
        ComboBox comboVilleArrive = peupleComboBox(ITEM_COMBOBOX_DIST_VILLE);
        comboVilleArrive.setUserData("comboVilleArrive");
        comboVilleArrive.setValue("Choisi la seconde ville.");
        comboVilleArrive.setOnAction(VBoxRoot.getControleur());
        // HBox contenant les 2 ComboBox pour la distance entre 2 villes
        HBox boiteDistVille = new HBox();
        boiteDistVille.getChildren().addAll(comboVilleDepart, comboVilleArrive);


        /* Le text area permettant d'afficher la distance entre les 2 villes choisies */
        areaTextDistVille = new TextArea();
        areaTextDistVille.setWrapText(true);
        areaTextDistVille.setPrefRowCount(3);
        areaTextDistVille.setPrefColumnCount(20);


        /* VBox contenant ComboBox et text area*/
        VBox boiteDistance = new VBox();
        boiteDistance.getChildren().addAll(labelExplicationIti, comboItineraire, areaTextItineraire, boiteDistVille, areaTextDistVille);
        boiteDistance.setSpacing(15);


        /*  Image pierre */
        Image imagePokemonPierre = new Image("pierre.png");
        ImageView pokemonViewPierre = new ImageView(imagePokemonPierre);
        pokemonViewPierre.setFitHeight(420);
        pokemonViewPierre.setFitWidth(200);


        // Ajout à la fenêtre
        this.getChildren().addAll(pokemonViewPierre, boiteDistance);
    }

    private ComboBox peupleComboBox(String [] strings) {
        /* rempli la ComboBox avec les élements de l'interface ConstantesComboDistances*/
        ComboBox <String> comboBox = new ComboBox<>();
        for (String string : strings) {
            comboBox.getItems().add(string);
        }
        return comboBox;
    }


    public void ajoutVille(String ville){
        /*ajoute une ville au tableau des noms de ville*/
        nomVille.add(ville);
    }

    public void ajoutDistance(ArrayList distance){
        /*Méthode utilisée lors de la lecture du fichier de distance
        elle ajoute les distances d'une ville a la matrice de distances
        Paramètre: une list equi correspond a une ligne du fichier de distance*/
        tabDistances.add(distance);
    }



    public static HBoxDistances lectureDistance(File fichier) throws IOException {
        /*La fonction permet de lire le fichier de distance, et le convertir en une matrice (tableau 2D)
         qui résume la distances qui sépare chaque ville avec les autres*/

        HBoxDistances tabDistances = new HBoxDistances(); // on crée la matrice de distances

        BufferedReader bufferEntree = new BufferedReader(new FileReader(fichier));
        String ligne;
        StringTokenizer tokenizer;
        do {
            ligne = bufferEntree.readLine();
            if (ligne != null) {
                tokenizer = new StringTokenizer(ligne, " ");

                tabDistances.ajoutVille(tokenizer.nextToken()); // on ajoute le 1er élément de la ligne (nom ville (STR))
                //au tableau de ville

                ArrayList subDist = new ArrayList();  //on créer un sous tableau de distance pour chaque ville

                while (tokenizer.hasMoreTokens()) {
                    subDist.add(tokenizer.nextToken()); //On ajoute au sous tableau le reste de la ligne (distance(INT))
                }
                tabDistances.ajoutDistance(subDist);  //On ajoute le sous tableau à la matrice de distances
            }
        }
        while (ligne != null);
        bufferEntree.close();
        return tabDistances;  //On retourne la matrice de distances
    }




    public Object getDistance(String ville1, String ville2){

        /*Renvoi la distance entre deux villes choisies*/

        Object  dis;
        int indic1 = Math.abs(nomVille.indexOf(ville1));  //on récupère l'indice de la premiere ville choisie
        int indic2 = Math.abs(nomVille.indexOf(ville2));  //on récupère l'indice de la deuxième ville choisie
        dis = tabDistances.get(indic1).get(indic2) ;  //on récupère la valeur de la distance dans la matrice
        return  dis;
    }


    /*ACCESSEURS*/
    public List getVille (){
        /*renvoi la liste des ville associé au fichier distance*/
        return nomVille;
    }

    public List getTabDistances (){
        /*renvoi la matrice des distances associée au fichier distance*/
        return tabDistances;
    }

    public TextArea getAreaTextItineraire(){
        /* renvoi le text area associé a l'itinéraire choisi*/
        return areaTextItineraire;
    }

    public TextArea getAreaTextDistVille(){
        /* renvoi le text area associé a la distance entre 2 villes*/
        return areaTextDistVille;
    }

    /*AFFICHAGE*/

    public String toString(){
        return tabDistances + "\n" + nomVille;
    }


}
