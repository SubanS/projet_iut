package vue;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import modele.ConstantesComboScenario;

import java.io.*;
import java.util.*;



public class HBoxScenario extends HBox implements ConstantesComboScenario {

    List vendeurs;
    List acheteurs;
    List membres;
    TextArea areaTextScenario;

    public HBoxScenario() {
        /*L'objet Scénario est caractérisé par trois listes: vendeurs, acheteurs, et membres*/

        vendeurs = new ArrayList<>();  //liste des vendeurs
        acheteurs = new ArrayList<>();  // liste des acheteurs
        membres =  new ArrayList<>();  // liste des membres presents

        /* Label Explication */
        Label labelExplicationScen = new Label("Te voilà à présent au début de ton voyage." + "\n" +
                "Tu peux à présent choisir, parmi différentes possibilités, le scénario que tu souhaites afin de commencer ton aventure."+ "\n" +
                "Choisi en un, et sélectionne le menu itinéraire afin de savoir la prochaine étape de ton voyage.")
        ;

        /* ComboBox permettant de choisir le scenario voulu*/
        ComboBox comboScenario = peupleComboBox(ITEM_COMBOBOX_SCENARIO);
        comboScenario.setUserData("comboScenario");
        comboScenario.setValue("Choisis un scénario.");
        comboScenario.setOnAction(VBoxRoot.getControleur());

        /* Le text area permettant d'afficher la lecture du fichier scenario  */
        areaTextScenario = new TextArea();
        areaTextScenario.setWrapText(true);


        /* VBox contenant combobox et text area*/
        VBox boiteScenario = new VBox();
        boiteScenario.getChildren().addAll(labelExplicationScen,comboScenario, areaTextScenario);
        boiteScenario.setSpacing(24);

        /*  Image sacha */
        Image imagePokemonSacha = new Image("Sacha.png");
        ImageView pokemonViewSacha = new ImageView(imagePokemonSacha);

        // Ajout à la fenêtre
        this.getChildren().addAll(pokemonViewSacha, boiteScenario);
    }

    public void ajoutVendeurAcheteur(String vendeur, String acheteur) {
        vendeurs.add(vendeur);  // on ajoute une valeur a la liste de vendeurs
        acheteurs.add(acheteur); // on ajoute une valeur a la liste d'acheteur

        if (!membres.contains(vendeur)){
            membres.add(vendeur);
        }                                   /*On rempli la liste des memebres en
                                            faisant en sorte qu'il n'y ai pas de doublon*/
        if (!membres.contains(acheteur)){
            membres.add(acheteur);
        }
    }

    private ComboBox peupleComboBox(String [] strings) {
        /* rempli la ComboBox avec les élements de l'interface ConstantesComboScenario*/
        ComboBox <String> comboBox = new ComboBox<>();
        for (String string : strings) {
            comboBox.getItems().add(string);
        }
        return comboBox;
    }

    public static HBoxScenario lectureScenario(File fichier) throws IOException {
        /*
        La fonction permet de lire le fichier de scénario.
        * Paramètre: fichier texte qui contient une liste de transaction de la forme acheteur -> vendeur
        * Retour: Objet de type Scénario
        */

        HBoxScenario scenario = new HBoxScenario();

        BufferedReader bufferEntree = new BufferedReader(new FileReader(fichier));
        String ligne;

        StringTokenizer tokenizer;
        do {
            ligne = bufferEntree.readLine();
            if (ligne != null) {
                tokenizer = new StringTokenizer(ligne, " ->");
                scenario.ajoutVendeurAcheteur(tokenizer.nextToken(), tokenizer.nextToken());  // on ajoute a chacune des listes un cote de la liste
                // un cote vendeur et l'autre acheteur
            }
        }
        while (ligne != null);
        bufferEntree.close();

        return scenario;
    }

    public static void ecritureScenario(String nomFichiers, HBoxScenario HBoxScenario) throws IOException {
        /*permet de recréer un scénario à partir d'un fichier texte*/
        PrintWriter sortie = new PrintWriter(new BufferedWriter(new FileWriter(nomFichiers)));
        int i = 0;
        for (Object vendeur : HBoxScenario.getVendeurs()) {
            sortie.println(vendeur + " -> " + HBoxScenario.getAcheteurs().get(i));
            i++;
        }
        sortie.close();
    }

    /*ACCESSEURS*/

    public List getVendeurs() {
        /*On renvoi la liste des venderus*/
        return vendeurs;
    }

    public List getAcheteurs() {
        /*On renvoi la liste des acheteurs*/
        return acheteurs;
    }

    public List getMembres(){
        /*On renvoi la liste des membres*/
        return membres;
    }

    public TextArea getAreaTextScenario(){
        /* renvoi le text area associé au fichier scenario*/
        return areaTextScenario;
    }

    /*AFFICHAGE*/
    public String toString() {
        return "Voici la liste des Vendeurs : " + "\n" + vendeurs + "\n" + "\n" +
                "Voici la liste des Acheteurs : " + "\n" + acheteurs + "\n" + "\n" +
                "Voici la liste des Membres (que vous pouvez retrouver en détail dans la partie Membre, accessible depuis le menu) : " + "\n" + membres
        ;
    }
}