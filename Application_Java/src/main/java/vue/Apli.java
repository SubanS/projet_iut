package vue;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;

public class Apli extends Application {


    public void start(Stage stage) throws IOException {
        VBoxRoot root = new VBoxRoot();
        Scene scene =new Scene(root, 1200, 600);
        File css = new File ("css" + File.separator + "CSS_APLI.css");
        scene.getStylesheets().add(css.toURI().toString());
        stage.setScene(scene);
        stage.getIcons().add(new Image("iconePokemon.png"));
        stage.setTitle("APLI");
        stage.show();
    }
    public static void main(String[] args) {
        Application.launch();
    }


}