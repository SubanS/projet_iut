package vue;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

public class HBoxPageAccueil extends HBox{

    private Label labelBienvenue;

    public HBoxPageAccueil(){

        // Creation element graphique
        labelBienvenue = new Label("Bienvenue dans ton application." + "\n" + "\n"
                + "Tu trouveras ici toutes les informations relatives à votre livraison de carte Pokémon. " + "\n"
                + "\n" + "Pour cela, voici les étapes à suivre : " + "\n"
                + "- Dans le menu, choisi scénario, puis dans la nouvelle page, choisi le scénario voulu. " + "\n"
                + "- Puis, à nouveau dans le menu sélectionne itinéraire, puis dans la nouvelle page, sélectionne un type d'itinéraire." + "\n"
                + "\n" + "Tu as donc l'itinéraire pour ta livraison de carte Pokémon." + "\n"
                + "\n" + "Dans chaque menu, tu rencontreras un nouveau personnage, avec des informations complémentaires, t'aidant à faire ta décision."
                + "\n" +  "\n" + "Bonne Aventure." )
        ;

        //Image Professeur Chen
        Image imagePokemonChen = new Image("ProffChen.png");
        ImageView imagePokemonView = new ImageView(imagePokemonChen);

        // Ajout à la fenêtre
        this.getChildren().addAll(imagePokemonView, labelBienvenue);
    }
}

