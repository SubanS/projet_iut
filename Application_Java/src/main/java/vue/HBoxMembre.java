package vue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import modele.ConstantesComboMembre;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class HBoxMembre extends HBox implements ConstantesComboMembre {
    TreeMap <String,String> tabMembre;  //map avec en clé nom du membre et en valeur sa ville
    private TextArea areaTextMembre;
    private TextArea areaTextEcrireMembre;
    private TextArea areaTextAffichageVille;
    private String texte;



    public HBoxMembre(){
        /*
        Un objet de la classe membre est caractérisé par un dicctionnaire
         qui prend en clé le nom et en valeur la ville
         */
        tabMembre = new TreeMap<>();


        /* Label Explication pour avoir les membres d'une ville */
        Label labelExplicationMembreVille = new Label("Dans cette page tu peux retrouver des informations complémentaires sur ton voyage." + "\n" +
                "Les membres, ainsi que leur ville sont résumés dans cette page, ce qui te permettra de te préparer avant chaque rencontre.")
        ;

        /* ComboBox permettant de visualiser les membres de la ville choisi */
        ComboBox comboVille = peupleComboBox(ITEM_COMBOBOX_MEMBRE);
        comboVille.setValue("Choisis une ville.");
        comboVille.setUserData("comboVille");
        comboVille.setOnAction(VBoxRoot.getControleur());


        /* Le text area permettant d'afficher la lecture du fichier membre  */
        areaTextMembre = new TextArea();
        areaTextMembre.setWrapText(true);
        areaTextMembre.setPrefRowCount(3);
        areaTextMembre.setPrefColumnCount(15);


        /* Label Explication pour avoir la ville d'un membre */
        Label labelExplicationVilleMembre = new Label("Tu peux également retrouver la ville d'un membre." + "\n" +
                "Pour cela, écrit le nom d'un membre dans la première zone de texte ci-dessous, et appui sur rechercher, afin d'avoir la ville")
        ;


        // Text area permettant d'écrire le nom d'un membre
        areaTextEcrireMembre = new TextArea();
        areaTextEcrireMembre.setWrapText(true);
        areaTextEcrireMembre.setPrefWidth(130);
        areaTextEcrireMembre.setPrefHeight(24);


        // Bouton permettant de cherhcer la ville d'un membre
        Button boutonRechercher = new Button("_"+"Rechercher");
        boutonRechercher.setMnemonicParsing(true);


        // Text area permettant d'afficher la ville d'un membre
        areaTextAffichageVille = new TextArea();
        areaTextAffichageVille.setWrapText(true);
        areaTextAffichageVille.setPrefWidth(300);
        areaTextAffichageVille.setPrefHeight(24);

        // boite conteneur
        HBox boiteText = new HBox();
        boiteText.getChildren().addAll(areaTextEcrireMembre,boutonRechercher, areaTextAffichageVille);
        boiteText.setSpacing(5);

        /* VBox contenant combobox et text area*/
        VBox boiteMembre = new VBox();
        boiteMembre.getChildren().addAll(labelExplicationMembreVille, comboVille, areaTextMembre,labelExplicationVilleMembre, boiteText);
        boiteMembre.setSpacing(10);

        /*  Image Miaouss */
        Image imagePokemonMiaouss = new Image("miaouss.png");
        ImageView pokemonViewMiaouss = new ImageView(imagePokemonMiaouss);
        pokemonViewMiaouss.setFitHeight(400);
        pokemonViewMiaouss.setFitWidth(250);


        // Ajout à la fenêtre
        this.getChildren().addAll(pokemonViewMiaouss, boiteMembre);



        // Rechercher la ville d'un membre, en récupérant valeur areaTextEcrireMembre
        boutonRechercher.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                texte = areaTextEcrireMembre.getText();
                HBoxMembre membreRoot = VBoxRoot.getMembre();
                areaTextAffichageVille.setText("La ville du membre " + texte + " est " + membreRoot.getVilleMembre(texte) );
            }
        });


    }

    public void ajoutMembre(String nom, String ville){
        /* complète la map avec un membre et sa ville*/
        tabMembre.put(nom,ville);
    }

    public static HBoxMembre lectureMembre(File fichier) throws IOException {
        /*La fonction permet de lire le fichier de membres.
          Paramètre: fichier texte qui contient une liste de membres avec leur ville
          Retour: Objet de type Membre*/


        HBoxMembre membre = new HBoxMembre();
        BufferedReader bufferEntree = new BufferedReader(new FileReader(fichier));
        String ligne;

        StringTokenizer tokenizer;
        do {
            ligne = bufferEntree.readLine();
            if (ligne != null) {
                tokenizer = new StringTokenizer(ligne, " ");
                membre.ajoutMembre(tokenizer.nextToken(), tokenizer.nextToken()); //On ajoute un nouveau couple (membre,ville) avec les deux moitiés de la ligne
            }
        }
        while (ligne != null);
        bufferEntree.close();

        return membre;
    }

    private ComboBox peupleComboBox(String [] strings) {
        /* rempli la ComboBox avec les élements de l'interface ConstantesComboScenario*/
        ComboBox <String> comboBox = new ComboBox<>();
        for (String string : strings) {
            comboBox.getItems().add(string);
        }
        return comboBox;
    }

    public String getVilleMembre(String membre){
        /*renvoie la ville du membre choisi*/
        return tabMembre.get(membre);
    }

    public String getMembreVille(String ville){
        //renvoi les membres d'une ville
        ArrayList<String> retour = new ArrayList<>();
        for (String membre : tabMembre.keySet()){
            if (tabMembre.get(membre).equals(ville)){
                retour.add(membre);
            }
        }
        return  "Voici la liste des membres appartenant à la ville " + ville + " : " + "\n" +  retour;
    }


    public TextArea getAreaTextMembre(){
        /* renvoi le text area associé au fichier membre*/
        return areaTextMembre;
    }


    /*AFFICHAGE*/
    public String toString(){
        return tabMembre.toString();
    }






}
